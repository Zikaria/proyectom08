package com.example.myapplication.ui.main;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CountriesAPI {
    private final ArrayList<Countries> countries= new ArrayList<>();
    private final String BASE_URL = "https://restcountries.eu/";

    ArrayList<Countries> getCountries() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("rest")
                .appendPath("v2")
                .build();
        String url = builtUri.toString();
        System.out.println(url+" Zikaria Muhammad Parveen");
        return doCall(url);
    }

    private ArrayList<Countries>  doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return countries;
    }
    private ArrayList<Countries> processJson(String jsonResponse) {

        try {
            String continent =MainFragment.continent;
            JSONArray data = new JSONArray(jsonResponse);
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsonCountry = data.getJSONObject(i);
                Countries country = new Countries();
                if(jsonCountry.getString("region").equals(continent)){
                country.setName(jsonCountry.getString("name"));
                country.setCapital(jsonCountry.getString("capital"));
                country.setPopulation(jsonCountry.getString("population"));
                country.setArea(jsonCountry.getString("area"));
                    if(jsonCountry.has("flag")){
                        country.setFlag(jsonCountry.getString("flag"));
                    }
                    else {
                        country.setFlag("");
                    }
                    if(jsonCountry.has("region")){
                        country.setContinent(jsonCountry.getString("region"));
                    }
                    else {
                        country.setContinent("");
                    }

                    countries.add(country);
                }


                else if(continent.isEmpty()){
                    country.setName(jsonCountry.getString("name"));
                    country.setCapital(jsonCountry.getString("capital"));
                    country.setPopulation(jsonCountry.getString("population"));
                    country.setArea(jsonCountry.getString("area"));
                        if(jsonCountry.has("flag")){
                            country.setFlag(jsonCountry.getString("flag"));
                        }
                        else {
                            country.setFlag("");
                        }
                    if(jsonCountry.has("region")){
                        country.setContinent(jsonCountry.getString("region"));
                    }
                    else {
                        country.setContinent("");
                    }

                    countries.add(country);
                }




            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        verificar(countries);

        return countries;
    }
    private void verificar(ArrayList<Countries> countries) {
        System.out.println(countries.size()+ "LONGITUD ARRAY");
        System.out.println(countries);

    }
}
