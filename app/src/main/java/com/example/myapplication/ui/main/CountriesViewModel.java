package com.example.myapplication.ui.main;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

public class CountriesViewModel extends AndroidViewModel {

        private final Application app;
        private final AppDatabase appDatabase;
        private final CountryDAO countryDAO;
        private ArrayList<Countries> countries=new ArrayList<>();;

        public CountriesViewModel(Application application) {
            super(application);

            this.app = application;
            this.appDatabase = AppDatabase.getDatabase(
                    this.getApplication());
            this.countryDAO = appDatabase.getCountryDao();
        }

        public LiveData<List<Countries>> getCountries() {
            return countryDAO.getCountries();
        }
        public LiveData<List<Countries>> getCountries_filter() {
            return countryDAO.getCountries_filter(MainFragment.continent);
        }



    public void reload() {
            // do async operation to fetch users
            RefreshDataTask task = new RefreshDataTask();
            task.execute();
        }

        private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                CountriesAPI api = new CountriesAPI();
                //Funciona porque si ya hay datos en la array, no llama a  la api de nuevo para coger datos.
                if(countries.size()==0) {
                    countries = api.getCountries();
                }
                countryDAO.deleteCountries();
                countryDAO.addCountries(countries);

                return null;
            }

        }

    }

