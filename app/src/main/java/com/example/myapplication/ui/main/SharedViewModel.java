package com.example.myapplication.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Countries> selected = new MutableLiveData<Countries>();

    public void select(Countries country) {
        selected.setValue(country);
    }

    public LiveData<Countries> getSelected() {
        return selected;
    }
}