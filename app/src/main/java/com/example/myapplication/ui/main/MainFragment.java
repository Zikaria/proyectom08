package com.example.myapplication.ui.main;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.example.myapplication.R;
import com.example.myapplication.databinding.MainFragmentBinding;

import java.util.ArrayList;
public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private CountriesViewModel model;
    private ArrayList<Countries> items;
    private SharedViewModel sharedModel;
    private MainFragmentBinding binding;
    public static String continent;

    public static MainFragment newInstance() {
        return new MainFragment();
    }


    private CountryAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater);
        View view=binding.getRoot();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        continent = preferences.getString("continent","");
        Toast.makeText(getContext(),continent, Toast.LENGTH_LONG).show();


          items  = new ArrayList<>();

        adapter = new CountryAdapter(
                getContext(),
                R.layout.lv_countries_row,
                items
        );
        sharedModel = ViewModelProviders.of(getActivity()).get(
                SharedViewModel.class
        );
        binding.lvCountries.setAdapter(adapter);
        binding.lvCountries.setOnItemClickListener((adapter, fragment, i, l) -> {
            Countries country = (Countries) adapter.getItemAtPosition(i);
            if(!esTablet()) {
                Intent intent = new Intent(getContext(), ZikaActivity.class);
                intent.putExtra("country", country);

                startActivity(intent);
            }
            else {
                sharedModel.select(country);
            }
        });



        model = ViewModelProviders.of(this).get(CountriesViewModel.class);
        ArrayList<Countries> countries_filtro= new ArrayList<>();

        model.getCountries().observe(getViewLifecycleOwner(), countries -> {
            adapter.clear();
            if(!continent.isEmpty()) {
                for (int i = 0; i < countries.size(); i++) {
                    if (countries.get(i).getContinent().equals(continent)) {
                        countries_filtro.add(countries.get(i));
                    }
                }
                adapter.addAll(countries_filtro);
            }
            else {
                adapter.addAll(countries);
            }

        });


        return view;
    }
    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Countries>> {
        @Override
        protected ArrayList<Countries> doInBackground(Void... voids) {
            CountriesAPI api = new CountriesAPI();
            ArrayList<Countries> result = api.getCountries();
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Countries> countries) {
            adapter.clear();
            for (Countries country : countries) {
                adapter.add(country);
            }
        }
    }
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_countries_fragment, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void refresh()
    {        RefreshDataTask task = new RefreshDataTask();
        task.execute();
        model.reload();
    }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }
}