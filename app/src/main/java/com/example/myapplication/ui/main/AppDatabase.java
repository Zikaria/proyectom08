package com.example.myapplication.ui.main;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Countries.class}, version = 4)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class, "db"
                    ).fallbackToDestructiveMigration()
                    .fallbackToDestructiveMigrationOnDowngrade()
                    .fallbackToDestructiveMigrationFrom(2).build();
        }

        return INSTANCE;
    }

    public abstract CountryDAO getCountryDao();
}