package com.example.myapplication.ui.main;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.databinding.LvCountriesRowBinding;
import com.example.myapplication.ui.main.Countries;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;

import java.util.List;

public class CountryAdapter extends ArrayAdapter<Countries> {
    public CountryAdapter(Context context, int resource, List<Countries> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Obtenim l'objecte en la possició corresponent
        Countries country = getItem(position);

        LvCountriesRowBinding binding = null;

        // Mirem a veure si la View s'està reusant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_countries_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }

        // Unim el codi en les Views del Layout



        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        binding.tvName.setText(country.getName());
        GlideToVectorYou
                .init()
                .with(getContext()).load(
                Uri.parse(country.getFlag()),  binding.ivFlag);

        // Retornem la View replena per a mostrarla
        return binding.getRoot();
    }
}