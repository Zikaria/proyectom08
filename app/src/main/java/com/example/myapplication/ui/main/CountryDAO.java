package com.example.myapplication.ui.main;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
@Dao
public interface CountryDAO {

    @Query("select * from countries")
    LiveData<List<Countries>>getCountries();

    @Query("select * from countries where continent = :continent " )
    LiveData<List<Countries>>getCountries_filter(String continent);

    @Insert
    void addCountry(Countries country);

    @Insert
    void addCountries(List<Countries> Countries);

    @Delete
    void deleteContry(Countries country);

    @Query("DELETE FROM countries")
    void deleteCountries();
}
