package com.example.myapplication.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



import com.example.myapplication.R;
import com.example.myapplication.databinding.ZikaMainFragmentBinding;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;

public class ZikaFragment extends Fragment {
    private View view;
    private ZikaViewModel mViewModel;
    private ZikaMainFragmentBinding binding;

    public static ZikaFragment newInstance() {
        return new ZikaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ZikaMainFragmentBinding.inflate(inflater);
        view=binding.getRoot();
        Intent i = getActivity().getIntent();

        if (i != null) {
            Countries country = (Countries) i.getSerializableExtra("country");
            System.out.println("INTENTANDO: " + country );
            if (country != null) {
                updateUi(country);
            }
        }
        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(getViewLifecycleOwner(), new Observer<Countries>() {
            @Override
            public void onChanged(@Nullable Countries country) {
                updateUi(country);
            }
        });



        return view;
    }
    private void updateUi(Countries country) {
        Log.d("COUNTRY", country.toString());

        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        System.out.println("INTENTANDO HACER SETS PARA ENSEÑAR ELPAIS");
        binding.tvName2.setText(country.getName());
        binding.tvCapital2.setText("Capital: "+country.getCapital());
        binding.tvArea2.setText("Area: "+country.getArea()+"km2");
        binding.tvPopulation2.setText("Population: "+country.getPopulation());
        GlideToVectorYou
                .init()
                .with(getContext()).load(
                Uri.parse(country.getFlag()), binding.ivFlag2);

    }


}